import jaydebeapi as jdbc


def return_last_comment_from_db():
    sql = 'SELECT * FROM wp_comments;'
    mysql_class = "com.mysql.cj.jdbc.Driver"
    mysql_jdbc_path = "/home/andrey/PycharmProjects/wordpress/helpers/mysql-connector-java-8.0.23.jar"
    mysql_url = "jdbc:mysql://localhost:3306/wp_database"
    mysql_user = "wp_user"
    mysql_pw = "password"

    conn = jdbc.connect(mysql_class,
                        mysql_url,
                        [mysql_user, mysql_pw],
                        mysql_jdbc_path)

    curs = conn.cursor()

    curs.execute(sql)
    result = curs.fetchall()
    curs.close()
    conn.close()

    return result[-1][8]


def insert_comment_to_db(text):
    sql = f"""INSERT INTO wp_database.wp_comments (comment_post_ID, comment_author, comment_author_email, 
                                                  comment_author_url, comment_author_IP, comment_date, comment_date_gmt, 
                                                  comment_content, comment_karma, comment_approved, comment_agent, 
                                                  comment_type, comment_parent, user_id) VALUES 
                                                  (8, 'Андрей', 'sidorov.avk@gmail.com', '', '127.0.0.1', 
                                                  '2021-04-09 09:52:10', '2021-04-09 09:52:10', '{text}',
                                                   0, 1, 'python-requests/2.25.1', 'comment', 0, 0);"""

    mysql_class = "com.mysql.cj.jdbc.Driver"
    mysql_jdbc_path = "/home/andrey/PycharmProjects/wordpress/helpers/mysql-connector-java-8.0.23.jar"
    mysql_url = "jdbc:mysql://localhost:3306/wp_database"
    mysql_user = "wp_user"
    mysql_pw = "password"

    conn = jdbc.connect(mysql_class,
                        mysql_url,
                        [mysql_user, mysql_pw],
                        mysql_jdbc_path)

    curs = conn.cursor()

    curs.execute(sql)
    curs.close()
    conn.close()
