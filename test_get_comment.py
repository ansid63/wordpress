import requests
from test_data.data import CommentData
from helpers.jdbc_helper import insert_comment_to_db


class TestApiGetCommentAddedToDb:
    def test_api_get_comment_added_to_db(self):
        text = CommentData.text_comment
        insert_comment_to_db(text)
        response_get_comment = requests.get(CommentData.URL_MAIN_PAGE)
        assert response_get_comment.status_code == 200, \
               f'В ответе на отправку комментария пришел статус код {response_get_comment.status_code}'

        assert text in response_get_comment.text, "Комментарий из базы данных не вернулся через API"
