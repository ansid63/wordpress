import requests
from test_data.data import CommentData


class TestApiAddComment:
    def test_api_add_comment(self):
        response_add_comment = requests.post(CommentData.URL_COMMENT, data=CommentData.COMMENT)
        assert response_add_comment.status_code == 200, \
            f'В ответе на отправку комментария пришел статус код {response_add_comment.status_code}'

        response_main_page = requests.get(CommentData.URL_MAIN_PAGE)
        assert CommentData.text_comment in response_main_page.text, "Ошибка в комментарии на главной странице"
