from faker import Faker


class CommentData:
    URL_COMMENT = "http://localhost/wp-comments-post.php"
    URL_MAIN_PAGE = "http://localhost/?page_id=8"

    fake = Faker('ru_RU')
    text_comment = fake.text(max_nb_chars=30)
    COMMENT = {'comment': text_comment,
               'author': 'Андрей',
               'email': 'sidorov.avk@gmail.com',
               'url': '',
               'submit': 'Отправить комментарий',
               'comment_post_ID': '8',
               'comment_parent': '0'}
