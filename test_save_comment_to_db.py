import requests
from test_data.data import CommentData
from helpers.jdbc_helper import return_last_comment_from_db


class TestApiAddComment:
    def test_api_save_comment_to_db(self):
        response_add_comment = requests.post(CommentData.URL_COMMENT, data=CommentData.COMMENT)
        assert response_add_comment.status_code == 200, \
            f'В ответе на отправку комментария пришел статус код {response_add_comment.status_code}'

        last_comment_db = return_last_comment_from_db()
        assert CommentData.text_comment == last_comment_db, "Ошибка в комментарии в базе данных"
